#t02_02.2_grados

print("CONVERSOR DE GRADOS CELSIUS A GRADOS FAHRENHEIT\n")

ingcelsius = input("Introduce una temperatura en grados Celsius: ")

try:
	gcelsius = float(ingcelsius)
	gfahren = 1.8*gcelsius+32
	print("{0:.1f}".format(gcelsius)+" ºC son "+"{0:.1f}".format(gfahren)+" ºF ")
except:
	print("Introduce un dato válido") 