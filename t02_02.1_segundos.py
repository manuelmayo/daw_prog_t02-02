#t02_02.1_segundos

print("Conversor a segundos\n")

d = input("Número de días: ")
h = input("Número de horas: ")
m = input("Número de minutos: ")
s = input("Número de segundos: ")

f = 60 #Segundos que ten un minuto
f2 = 60*60 #Segundos que ten unha hora
f3 = 60*60*24 #Segundos que ten un día

if (d.isnumeric() and h.isnumeric() and m.isnumeric() and s.isnumeric()):
	#Convertimos os Inputs en números
	d = int(d)
	h = int(h)
	m = int(m)
	s = int(s)
	#Segundos totales
	st = s+m*f+h*f2+d*f3
	#SALIDA
	print("")
	#print("%d días, %d horas, %d minutos y %d segundos son %d segundos" % (d, h, m, s, st))
	print("{0} días, {1} horas, {2} minutos y {3} segundos son {4} segundos".format(d, h, m, s, st))
else:
	print("Escribe ben os números") 