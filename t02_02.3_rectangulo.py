#t02_02.3_rectangulo

l1 = input("Introduce uno de los lados: ")
l2 = input("Introduce el otro lado: ")

print("")

try:
	l1 = float(l1)
	l2 = float(l2)
	perim = float(l1*2 + l2*2)
	area = float(l1*l2)
	perim_salida = "Perímetro del rectángulo: \t"+"{0:.2f}".format(perim)
	area_salida = "Área del rectangulo: \t\t"+"{0:.2f}".format(area)
	n_iguales = max([len(perim_salida),len(area_salida)+4])+5
	print("="*n_iguales)
	print(perim_salida)
	print(area_salida)
	print("="*n_iguales)
except:
	print("Escribe ben os números") 