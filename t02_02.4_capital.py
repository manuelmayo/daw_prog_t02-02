##t02_02.4_capital

c_inicial = input("Introduce el capital inicial: ")
int_anual = input("Introduce el interés anual (%): ")
n_anos = input("Introduce el número de años: ")

print("")

try:
	#Inputs en formato númerico
	c_inicial = float(c_inicial)
	int_anual = float(int_anual)
	n_anos = int(n_anos)
	#Calculos
	c_final = c_inicial * (1+int_anual/100)**n_anos
	rendimiento = c_final - c_inicial
	#Numeros formateados
	c_inicial_f = "{0:.2f}".format(c_inicial)
	int_anual_f = "{0:.2f}".format(int_anual)
	n_anos_f = str(n_anos)
	c_final_f = "{0:.2f}".format(c_final)
	rendimiento_f = "{0:.2f}".format(rendimiento)
	#Tamaño dos Numeros formateados
	c_inicial_len = len(c_inicial_f)
	int_anual_len = len(int_anual_f)
	n_anos_len = len(n_anos_f)
	c_final_len = len(c_final_f)
	rendimiento_len = len(rendimiento_f)
	#Tamaño maior
	max_f = max([c_inicial_len,int_anual_len,n_anos_len,c_final_len,rendimiento_len])
	#Diferencias co maios
	c_inicial_diff = max_f-c_inicial_len
	int_anual_diff = max_f-int_anual_len
	n_anos_diff = max_f-n_anos_len
	c_final_diff = max_f-c_final_len
	rendimiento_diff = max_f-rendimiento_len
	#Textos de salida
	salida_c_inicial = "# Capital inicial:\t"+" "*c_inicial_diff+c_inicial_f+" €"
	salida_int_anual = "# Interés anual:\t"+" "*int_anual_diff+int_anual_f+" %"
	salida_n_anos = "# Periodo:\t\t"+" "*n_anos_diff+n_anos_f+" años"
	salida_c_final = "# Capital final:\t"+" "*c_final_diff+c_final_f+" €"
	salida_rendimiento = "# Rendimiento:\t\t"+" "*rendimiento_diff+rendimiento_f+" €"
	#Cantidad de #
	s_long = len(salida_n_anos)+12+2
	#SALIDA
	print("#"*s_long)
	print("#"+" "*(s_long-2)+"#")
	print(salida_c_inicial+"{0:>5}".format("#"))
	print(salida_int_anual+"{0:>5}".format("#"))
	print(salida_n_anos+" #")
	print("#"+" "*(s_long-2)+"#")
	print(salida_c_final+"{0:>5}".format("#"))
	print(salida_rendimiento+"{0:>5}".format("#"))
	print("#"+" "*(s_long-2)+"#")
	print("#"*s_long)
except:
	print("Escribe ben os números") 